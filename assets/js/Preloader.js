Game.Preloader = function(game) {

	this.preloadBar = null;
	this.game = game;
};

Game.Preloader.prototype = {
	preload: function() {
		this.preloadBar = this.add.sprite(this.world.centerX,
										  this.world.centerY, 'preloaderBar');

		this.preloadBar.anchor.setTo(0.5, 0.5);
		this.time.advancedTiming = true;
		this.load.setPreloadSprite(this.preloadBar);

		// ASSETS

		//Load images
		this.load.tilemap('map', 'assets/map5.csv');
		this.load.image('tileset', 'assets/tile.png');
		this.load.image('background', 'assets/background.png');
		this.load.image('backgroundr', 'assets/grafik_bg.png');
		this.load.image('live', 'assets/olowek.png');
		
		this.load.image('green_p', 'assets/green_p.png');
		this.load.image('yellow_p', 'assets/yellow_p.png');
		this.load.image('rose_p', 'assets/rose_p.png');
		
		
		
		//load sound
		this.load.audio('jump', ['assets/audio/jump1.mp3']);
		this.load.audio('turboJump', ['assets/audio/turbo_jump.mp3']);
		this.load.audio('bonus', ['assets/audio/bonus.mp3']);
		this.load.audio('achive100', ['assets/audio/achive100.mp3']);		
		this.load.audio('game_over', ['assets/audio/game_over.mp3']);
		this.load.audio('start', ['assets/audio/start.mp3']);	
		
		this.load.spritesheet('platform', 'assets/brick_normal1.png', 189, 44);
        this.load.spritesheet('platformLong', 'assets/brick_lg1.png', 350, 44);
        this.load.spritesheet('platformShort', 'assets/brick_small1.png', 120, 44);

        this.load.spritesheet('coffee', 'assets/kawa.png', 53, 54);

		this.load.spritesheet('player', 'assets/postacie_ultimate_small_lot.png', 90, 132);
		this.load.spritesheet('programmer', 'assets/programista.png', 274, 274);

		this.load.spritesheet('dealine', 'assets/dedlajn_scaled.png', 167, 71);
		//this.load.spritesheet('dealine', 'assets/dedlajn_grafik_animacja.png', 400, 214);
		
		//this.sound.setDecodedCallback([ jump, turboJump, bonus,  achive100, game_over, start], start, this);
	},

	create: function() {
		$('body').stop();
		this.state.start('Level1');
	},
	
}
