Game.Coffee = function(level) {
	this.level = level;
    this.platformHandler = null;
    this.physics = null;
	this.playerHandler = null;
    this.coffeeGropus = null;
    this.coffee = null
    this.prevY = -2300;
    //this.prevY = 600;
    this.sound = null;
	this.showMessage = false;
	this.emitter = null;
    this.pMul = 1;
    this.coffeeNum = 0;
};

Game.Coffee.prototype = {

    create: function(platformHandler, physics, sound, playerHandler) {
        this.platformHandler = platformHandler;
        this.physics = physics;
		this.sound = sound;
		this.playerHandler = playerHandler;
		console.log(this.playerHandler);
        this.coffeeGropus = this.level.add.physicsGroup();		
		
		this.emitter = this.level.game.add.emitter(0, 0, 2000);
		this.emitter.makeParticles(['green_p', 'yellow_p', 'rose_p']);
        this.playerHandler.player.addChild(this.emitter);
		//this.emitter.minParticleSpeed.setTo(-1000, -1000);
		//this.emitter.maxParticleSpeed.setTo(1000, 1000);
        
        //this.emitter.lifespan = 500;
        //this.emitter.minParticleSpeed.setTo(-500, -500);
        //this.emitter.maxParticleSpeed.setTo(100, 100);
		//this.emitter.gravity = 0;
		
		this.initCoffee();

        //this.coffee = this.level.add.sprite(960, 560, 'coffee');
        //this.coffee.anchor.setTo(0.5, 0.5);
        //this.coffee.animations.add('stay', [0, 1], 3, true);
        //this.coffee.animations.play('stay');
        //this.addCoffee(800);
    },
	
	initCoffee: function() {
		for(var i = 0; i < 30; i++) {
			this.addCoffee(this.prevY);
			this.prevY -= 3000;
		}
	},

    reinit: function() {
		this.prevY = -2300;
        this.coffeeGropus.destroy();
        this.coffeeGropus = this.level.add.physicsGroup();
		this.initCoffee();
        this.pMul = 1;
        this.coffeeNum = 0;
    },

    addCoffee: function(y) {
        var x = this.level.rnd.integerInRange(400, 1550);
        var coffee = this.level.add.sprite(x, y, 'coffee');
        this.coffeeGropus.add(coffee);

        coffee.animations.add('stay', [0, 1], 3, true);
        coffee.animations.play('stay');

        this.physics.arcade.enable(coffee);
        coffee.body.allowGravity = false;
        coffee.body.immovable = true;
        this.coffeeNum++;
        if(this.coffeeNum >= 7 * this.pMul) {
            this.prevY -= 50;
            this.pMul += 1;
        }
    },

    update: function() {
        if(this.platformHandler.addCoffee) {
            var l = this.platformHandler.platforms.length;
            var platform = this.platformHandler.platforms.getChildAt(l - 1);
			this.addCoffee(this.prevY);
			this.prevY -= 3000;
			this.platformHandler.addCoffee = false;
        }
        var cameraY = this.level.camera.y;
        this.coffeeGropus.forEach(function(coffee) {
            if(coffee.y > cameraY + 1920) {
                coffee.destroy();
            }
        });
    },

    getCoffee: function(player, coffee) {
        //console.log(coffee);
        this.sound.turboJump();
        this.sound.bonus();
        coffee.destroy();
        player.body.velocity.y = -3000;
		this.playerHandler.isRunnig = true;
		this.playerHandler.runningTime = 5000;
		this.showMessage = true;
		
		//this.emitter.x = coffee.x;
		//this.emitter.y = coffee.y;
        this.emitter.x = 0;
        this.emitter.y = 0;

		//this.emitter.start(true, 2000, null, 500);
        //this.emitter.start(false, 5000, 20);
        //this.emitter.start(false, 5000, 20);
        //this.emitter.start(true, 2000, null, 500);

        var EXPLODE_DIAMETER = 100.0;
        for (var i = 0; i < 360; i=i+10) {
            var x = this.level.rnd.integerInRange(100, 500);
            var explode = x + EXPLODE_DIAMETER;
            var xsp = Math.cos(2 * Math.PI * i / 360.0) * explode;
            this.emitter.setXSpeed(xsp, xsp);
            var ysp = Math.sin(2 * Math.PI * i / 360.0) * explode;
            this.emitter.setYSpeed(ysp, ysp);
            this.emitter.start(true, 2000, null, 1);
            this.emitter.update();
        }
    }
}