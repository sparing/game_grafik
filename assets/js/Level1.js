Game.Level1 = function(game) {
    
};

var map;
var layer;
var percent;
var sound;
var bg;

Game.Level1.prototype = {
	init: function () {
		
        this.game.renderer.renderSession.roundPixels = true;

        //this.world.resize(640, 2000);

        this.physics.startSystem(Phaser.Physics.ARCADE);

        this.physics.arcade.gravity.y = 1000;
        this.physics.arcade.skipQuadTree = false;

        this.PlayerHandler = new Game.Player(this);
        this.ProgrammerHandler = new Game.Programmer(this);
        this.CameraHandler = new Game.Camera(this);
        //this.WorldHandler = new Game.World(this);
        this.PlatformHandler = new Game.Platform(this);
        this.StatusHandler = new Game.Status(this);
        this.WorldHandler = new Game.World(this);
        this.SoundHandler = new Game.Sound(this);
        this.CoffeeHandler = new Game.Coffee(this);
    },

	create: function() {
		//this.stage.backgroundColor = '#ffffff';
        // this.stage.alpha = 0.7;
        // bg = this.game.add.tileSprite(0, 0, 400, 400, 'backgroundr');
		
		// scaling
	    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	    this.scale.maxWidth = this.game.width;
	    this.scale.maxHeight = this.game.height;
	    this.scale.pageAlignHorizontally = true;
	    this.scale.pageAlignVertically = true;

        /*
            map = this.add.tilemap('map', 6, 6);
            map.addTilesetImage('tileset');
            layer = map.createLayer(0);
            layer.resizeWorld();

            map.setCollisionBetween(0, 0);
        */
               
            this.WorldHandler.create(this.physics);
            this.ProgrammerHandler.create();
            this.PlayerHandler.create(this.SoundHandler, this.ProgrammerHandler);
            this.SoundHandler.create();
            this.CameraHandler.create(this.PlayerHandler.player);
            this.PlatformHandler.create(this.PlayerHandler.player, this.physics);
            this.CoffeeHandler.create(this.PlatformHandler, this.physics, this.SoundHandler, this.PlayerHandler);

            this.StatusHandler.create(this.PlatformHandler, this.CameraHandler, this.CoffeeHandler, this.SoundHandler, this.ProgrammerHandler, this.PlayerHandler);

            this.physics.arcade.enable(this.PlayerHandler.player);
            this.physics.arcade.enable(this.StatusHandler.deadline);
	        this.PlayerHandler.setPhysics();

        //this.timer = this.time.events.loop(100, this.PlatformHandler.addRandomPlatform, this.PlatformHandler); 
	},

	setFriction: function (player, platform) {

        if (platform.key === 'platform')
        {
            player.body.x -= platform.body.x - platform.body.prev.x;
        }

    },

	update: function() {
		this.physics.arcade.collide(this.PlayerHandler.player, this.WorldHandler.map, this.setFriction, null, this);
		this.physics.arcade.collide(this.PlayerHandler.player, this.PlatformHandler.platforms, this.PlatformHandler.changePlatform, null, this.PlatformHandler);
        this.physics.arcade.collide(this.PlayerHandler.player, this.CoffeeHandler.coffeeGropus, this.CoffeeHandler.getCoffee, null, this.CoffeeHandler);
        this.physics.arcade.collide(this.PlayerHandler.player, this.StatusHandler.deadline, this.StatusHandler.deadlineCatch, null, this.StatusHandler);
		this.physics.arcade.collide(this.PlayerHandler.player, layer);
              
		this.PlayerHandler.player.body.velocity.x = 0;

		this.PlayerHandler.update();

		this.world.setBounds(0, - this.PlayerHandler.player.yChange, this.world.width, this.game.height + this.PlayerHandler.player.yChange);
 		this.SoundHandler.update();
		this.CameraHandler.update();
        this.WorldHandler.update();
		this.PlatformHandler.update();
        this.CoffeeHandler.update();
        this.StatusHandler.update();
	}
}
