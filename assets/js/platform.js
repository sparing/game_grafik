Game.Platform = function(level) {
	this.level = level;
	this.player = null;
	this.platforms = null;
    this.platformYPos = 800;
    this.offsetX = 310;
    this.prevX = 310;
    this.prevWidth = 350;
    this.platformWidth = 142;
    this.platformsNum = 0;
    this.platformsDone = 0;
    this.platformsScore = 0;
    this.touchedPlatform = -9999;
	this.addTimer = 0;
	this.addCoffee = false;
    this.space = 150;
    this.hSpace = 200;

    this.types = [
    	{'name': 'platformLong', 'width': 350},
    	{'name': 'platform', 'width': 190},
    	{'name': 'platformShort', 'width': 120},
    ];
};

Game.Platform.prototype = {

	create: function(player, physics) {
		this.player = player;
		this.physics = physics;

		this.platformYMin = 99999;

		this.platforms = this.level.add.physicsGroup();
		this.intPlatforms();
	},

	intPlatforms: function() {
		for(var i = 0; i < 100; i++) {
			this.addRandomPlatform();
		}
		//this.platforms.create(200, 900, 'platform');
        //this.platforms.create(600, 900, 'platform');

        this.platforms.setAll('body.allowGravity', false);
        this.platforms.setAll('body.immovable', true);
        this.platforms.setAll('body.checkCollision.down', false);
	},

    reinit: function() {
        this.platformYPos = 800;
        this.platformsNum = 0;
        this.platformsDone = 0;
        this.platformsScore = 0;
        this.touchedPlatform = -9999;
        this.space = 150;
        this.hSpace = 200;

       /* this.platforms.forEach(function(platform) {
            platform.destroy();
        });*/
        this.platforms.destroy();
        this.platforms = this.level.add.physicsGroup();
        this.intPlatforms();
    },

	addOnePlatform: function(x, y, type) {
    	var platform = this.level.add.sprite(x, y, this.types[type]['name']);
    	this.platforms.add(platform);

        platform.animations.add('coded', [0], 1, true);
        platform.animations.add('filled', [1], 1, true);

    	this.physics.arcade.enable(platform);
    	//platform.body.velocity.y = -200;

    	//platform.scale.x = scale;
    	//console.log(platform.width);

    	//platform.checkWorldBounds = true;
    	//platform.outOfBoundsKill = true;
    	platform.body.allowGravity = false;
        platform.body.immovable = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;

        this.platformsNum += 1;
		if(this.platformsNum % 15 == 0) {
			this.addCoffee = true;
		}

        if(this.platformsNum % 50 == 0) {
            this.space += 50;
        }
        if(this.platformsNum % 100 == 0) {
            this.hSpace += 50;
        }

        this.prevWidth = platform.width;
        this.prevX = platform.x;
        //console.log(this.prevWidth);
        //console.log(platform.width);
        //console.log(this.platformsNum);
        //console.log(this.level.game.width);
    },

    addRandomPlatform: function() {
    	var pos = this.level.rnd.integerInRange(0, 1);
    	var randWallOffset = this.level.rnd.integerInRange(10, 50);
    	var type = this.level.rnd.integerInRange(0, 2);
    	//var scale = this.level.rnd.realInRange(0.5, 2);
    	var w = this.types[type]['width'];
    	if(pos == 0) {
    		//left position
    		var x = this.level.rnd.integerInRange(this.prevX - w - this.space, this.prevX - w);
    	} else {
    		//right postion
			var x = this.level.rnd.integerInRange(this.prevX + this.prevWidth, this.prevX + this.prevWidth + this.space);
    	}
        //console.log('x ' + x);
        //console.log('w ' + w);
    	if(x < 310) {
    		x = 310 + randWallOffset;
    	} else if(x > 1610 - w) {
    		x = 1610 - w - randWallOffset;
    	}
    	//var x = Math.floor(Math.random() * 1300) + this.offsetX;
    	this.addOnePlatform(x, this.platformYPos, type);
    	this.platformYPos -= this.hSpace;
    },

	update: function() {		
		var cameraY = this.level.camera.y;
		this.platforms.forEach(function(platform) {
			if(platform.y > cameraY + 1920 && platform.health == 0) {
				platform.destroy();
			}
		});
	},

	changePlatform: function(player, platform) {
        platform.animations.play('filled');
        //platform.setHealth(0);

        if(this.touchedPlatform != platform.y) {
            if(this.touchedPlatform > platform.y) {
                if(platform.health == 1) {
                    this.platformsDone += 1;
                    this.platformsScore += 1;
                }

                var pDone = 0;
                this.platforms.forEach(function(p) {
                    if(platform.y < p.y) {
                        if(p.health == 1) {
                            pDone += 1;
                        }
                        p.animations.play('filled');
                        p.setHealth(0);
                    }
                });
                if(pDone != 1) {
                    this.platformsDone += pDone;
                    this.platformsScore += pDone * 2;
                }
				for(var i = 0; i < pDone; i++) {
					this.addRandomPlatform();
				}
            }
            this.touchedPlatform = platform.y;
        }
		
		//var loadY = this.level.camera.y - 10000;
		/*if(this.level.camera.y < this.addTimer) {
			console.log(this.level.camera.y);
			
			this.addTimer -= 50;
			this.addRandomPlatform();
		}*/
	}
}