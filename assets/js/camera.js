Game.Camera = function(level) {
	this.level = level;
	this.player = null;

};

var playerDeltaY;
var playerLastY;
var deltaTimer;
var playerLiveDelta;
var q;
var bonusQ; //wspolczynnik, przez ktory mnozymy q jesli chcemy spowolnic kamerę
var cameraStep; 
var stepTimer;
var playerFolowHeight;
var playerMaxY;
var gameOver;

Game.Camera.prototype = {

	init: function () {
	},

	create: function(player) {
		this.player = player;
	    this.cameraYMin = -this.level.game.height + this.player.y+200;
	    this.level.camera.y = Math.min(this.cameraYMin, this.player.y - this.level.game.height + 330);
	  
	  	this.gameOver = false;
		this.q =0;
		this.bonusQ =1;
		this.deltaTimer = this.level.time.now + 1000;
		this.cameraStep = 100;
		this.stepTimer = 20000;
		this.stepMul = 1;
		this.playerFolowHeight = 0;
		this.playerLastLiveY = this.player.y;
		this.playerMaxY = 0;
	},

	update: function() {		
		this.q = 0;
			
    	this.playerDeltaY = this.playerLastY - this.player.y;
    	this.playerLiveDelta = 0; 
		
		//STD STEP INDICATOR => Q = Step * bonusQ
		this.bonusQ = 0.004;
		
		this.playerLiveDelta = this.playerLastLiveY - this.player.y;
		
		
		//SLOW DOWN CAMERA INDICATOR if BONUS
		if(this.playerDeltaY > 250){
			this.bonusQ = 0;

    		//hard level
    		if(this.cameraStep > 150){
	    		this.cameraStep = this.cameraStep - 0.3;
    		}
    		
    		
    		if((this.cameraYMin - this.playerLiveDelta*0.7) < this.player.y - 200){
    			this.playerLiveDelta = this.playerLiveDelta *0.7;
    		}
    		
    	}
		
		
		//COUNT CAMERA SPEED UP INDICATOR
    	if(this.player.y < 0){
    		this.q = Math.abs(this.q) + this.cameraStep * this.bonusQ;
    	}
    	
		//MAIN SPEED-UP
		this.cameraYMin = this.cameraYMin - this.playerLiveDelta - this.q;
		//console.log(this.cameraYMin+ ' ' + this.level.game.height + ' ' + this.player.y);
		
		//console.log(this.cameraYMin+ ' ' +this.player.y );
	    this.level.camera.y = this.cameraYMin; //-this.level.game.height + this.player.y + this.playerLiveDelta + 430 - this.q;


		
		
		if(this.cameraYMin < (-this.level.game.height + this.player.y + 100) || (this.playerMaxY - Math.abs(this.player.y)) > 1220){
			this.gameOver = true;  
			//this.reinit();
			console.log('over');
		}
			    
	    if(this.level.time.now > this.deltaTimer){
	    	this.playerLastY = this.player.y;
	    	this.deltaTimer = this.level.time.now + 1000;
	    }
	    
	    if(this.level.time.now > this.stepTimer){
	    	this.cameraStep = this.cameraStep + 100 * this.stepMul;
	    	this.stepTimer = this.level.time.now + 20000;
			this.stepMul += 0.15;
			console.log('camera step: ' + this.cameraStep);
	    }
	    
	    this.playerLastLiveY = this.player.y;
	    
	    if(this.playerMaxY < Math.abs(this.player.y) && this.player.y < 0 ){
	    	this.playerMaxY = Math.abs(this.player.y);
	    }
	},
	
	reinit: function(){
		
	    this.cameraYMin = -this.level.game.height + this.player.y+430;
	    this.level.camera.y = Math.min(this.cameraYMin, this.player.y - this.level.game.height + 330);
	  
	  	this.gameOver = false;
		this.q =0;
		this.bonusQ =1;
		this.deltaTimer = this.level.time.now + 1000;
		this.cameraStep = 100;
		this.playerFolowHeight = 0;
		this.playerLastLiveY = this.player.y;
		this.playerMaxY = 0;
		this.player.y = 900;
		this.playerDeltaY = 0;
		this.playerLiveDelta = 0;
		this.stepMul = 1;
	}
	
	
}
