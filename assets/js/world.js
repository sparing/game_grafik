Game.World = function(level) {
	this.level = level;
        this.map = null;
};

Game.World.prototype = {
	init: function () {	
//		  this.level.game.load.image('flor', 'assets/flor.png');
		  this.level.game.load.atlasJSONHash('flor1', 'assets/tile_flor.png', 'assets/tile_flor.json');
	},

	create: function(physics) {
            this.physics = physics;


            // bottom
            var width = 1600 // example;
            var height = 130 // example;
            //var flor = new Tile(layer, index, x, y, width, height)
            var bottom = this.level.game.add.bitmapData(width, height);

            bottom.ctx.beginPath();
            bottom.ctx.rect(300, 00, width, height);
            bottom.ctx.fillStyle = '#000000';
            bottom.ctx.fill();
           
        
            
            this.map = this.level.add.physicsGroup();
            
          
            
            
            var bottomSprite = this.level.game.add.sprite(0, 1000, bottom);
            bottomSprite.anchor.setTo(0, 0);
            
            this.physics.arcade.enable(bottomSprite);
            
            bottomSprite.checkWorldBounds = true;
            bottomSprite.outOfBoundsKill = true;
            bottomSprite.body.allowGravity = false;
            bottomSprite.body.immovable = true;
            
            this.map.add(bottomSprite);
           // this.level.game.add.tileSprite(0, -100, 6, 7, 'flor1');
            
            // left
            var left = this.level.game.add.bitmapData(2, 1024);

            left.ctx.beginPath();
            left.ctx.rect(0, 0, 2, 1000);
            left.ctx.fillStyle = '#000000';
            left.ctx.fill();
            var leftSprite = this.level.game.add.sprite(300, 0, left);
            leftSprite.anchor.setTo(0, 0);
            leftSprite.fixedToCamera = true;
            this.physics.arcade.enable(leftSprite);
            
            leftSprite.checkWorldBounds = true;
            leftSprite.outOfBoundsKill = true;
            leftSprite.body.allowGravity = false;
            leftSprite.body.immovable = true;
            
            this.map.add(leftSprite);
          
            
            // right
            var right = this.level.game.add.bitmapData(2, 1024);

            right.ctx.beginPath();
            right.ctx.rect(0, 0, 2, 1080);
            right.ctx.fillStyle = '#000000';
            right.ctx.fill();
            var rightSprite = this.level.game.add.sprite(1600, 0, right);
            rightSprite.anchor.setTo(0, 0);
            rightSprite.fixedToCamera = true;
            this.physics.arcade.enable(rightSprite);
            
            rightSprite.checkWorldBounds = true;
            rightSprite.outOfBoundsKill = true;
            rightSprite.body.allowGravity = false;
            rightSprite.body.immovable = true;
            
            this.map.add(rightSprite);
            
           
	},

	update: function() {		
            
	}
}