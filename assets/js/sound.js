Game.Sound = function(level) {
	this.level = level;
	
	jump = this.level.game.add.audio('jump');
    turboJump = this.level.game.add.audio('turboJump');
    bonus = this.level.game.add.audio('bonus');
    achive100 = this.level.game.add.audio('achive100');
    game_over = this.level.game.add.audio('game_over');
    start = this.level.game.add.audio('start');
    
	this.level.game.sound.setDecodedCallback([ jump, turboJump, bonus, achive100, game_over, start], this.start, this);
	
};

Game.Sound.prototype = {
	init: function () {
		console.log('mam dzwiek');
	},
	
	create: function () {
		
	},
	
	update: function () {
		
	},

	jump: function() {
		jump.play();
	},

	turboJump: function() {
		turboJump.play();
	},
	
	bonus: function() {
		bonus.play();
	},
	
	achive100: function() {
		achive100.play();
	},
	
	gameOver: function() {
		game_over.play();
	},

	start: function() {
		start.play();
	},

}