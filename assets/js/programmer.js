Game.Programmer = function(level) {
	this.level = level;
	this.programmer = null;
	
};

Game.Programmer.prototype = {
	init: function () {

	},
	
	create: function () {
	
		this.programmer = this.level.add.sprite(15, 750, 'programmer');
		this.programmer.anchor.setTo(0, 0);
		this.programmer.fixedToCamera = true;
		
		this.programmer.animations.add('wait', [0, 1, 2], 3, true);
		this.programmer.animations.add('code', [3, 4, 5, 6], 4, true);
		this.programmer.animations.add('error', [7, 8], 6, true);
		this.programmer.animations.play('wait');
	},
	
	wait: function () {
		this.programmer.animations.play('wait');
	},

	code: function () {
		this.programmer.animations.play('code');
	},
	
	error: function () {
		this.programmer.animations.play('error');
	},

}