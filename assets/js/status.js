Game.Status = function(level) {
	this.level = level;
	this.platformHandler = null;
	this.cameraHandler = null;
	this.coffeeHandler = null;
	this.playerHandler = null;
	this.scoreLabel = null;
	this.bestScoreLabel = null;
	this.codedLabel = null;
	this.scoreText = null;
	this.bestscoreText = null;
	this.platformsText = null;
	this.score = 0;
	this.platforms = 0;
	this.platformsPercent = 0;
	this.lives = 3;
	this.livesGroup = null;
	this.scoreFrame = null;
	this.bestScore = 0;
	this.messages = [
		{p: 10, m: ['Jest nieźle!', 'Nawet mi się podoba!', 'Dla Ciebie to dwa kliki!']},
		{p: 40, m: ['Szybki jak przelew od klienta!', 'no to CMYK!', 'dobry jak logo pomorskiego!']},
		{p: 60, m: ['Poproszę efekt WOW!', 'Zrób tak żeby było ładnie!', 'nie płacz nad projektem!']},
		{p: 80, m: ['Jeszcze kilka poprawek!', 'Dobrze zaczynasz!', 'Dobrze zaczynasz!']},
		{p: 100, m: ['Poproszę nową wersję!', 'uwaga do wersji finalnej!', 'uwaga do wersji ostatecznej!']},
		{p: 110, m: ['Powiększ logo!', 'Daj logo na środek!', 'Wyśrodkuj logo do lewej!']},
		{p: 140, m: ['czcionka jest za mała!', 'Daj Lepszą czcionkę!', 'Czcionka się rozjechała!']},
		{p: 160, m: ['Dałbym trochę czerwieni!', 'Żona lubi zielony!', 'Niebieski daj bardziej maryjny!']},
		{p: 180, m: ['Czegoś mi tu brakuje...', 'Zrób tak, ale inaczej!', 'Targetem są wszyscy!']},
		{p: 200, m: ['Poproszę próbkę logotypu!', 'Będziesz miał do portfolio!', 'Moje oczy tego nie chłoną!']},
		{p: 210, m: ['Nie widzę jpegów!', 'Lorem ipsum to po jakiemu?', 'Za dużo tej pustki!']},
		{p: 240, m: ['Syn sąsiada zrobi w 5 minut!', 'Logo to 5 minut roboty!', 'Szwagier zrobi za flaszkę!']},
		{p: 260, m: ['Czy final_15 jest ostateczną wersją?', 'Czy wersja final_final jest finalna?', 'Czy wersja final_shit jest finalna?']},
		{p: 280, m: ['OK! daje akcept!', 'OK! Akceptuję projekt!', 'OK! Nie mam uwag!']},
		{p: 300, m: ['Brawo! Czelendżujesz na 1000%?', 'Master of dedlajn! 300% normy!', 'BRAWO! 300% normy!']}
	];
	this.endMessages = [
		'NO I KLOPS! JESTEŚ FAKAPMEJKEREM!',
		['BRAWO! JESTEŚ POSKRAMIACZEM DZIKICH DEDLAJNÓW!', 'BRAWO! JESTEŚ MASTEREM ASAPÓW!', 'BRAWO! JESTEŚ PRZODOWNIKIEM PRACY GRAFICZNEJ!', 'BRAWO! JESTEŚ SENIOR MASTEREM DEDLAJNÓW!',
		'BRAWO! JESTEŚ FIGHTEREM ASAPÓW!', 'BRAWO! POKONAŁEŚ DZIKI DEDLAJN WPROST EPICKO!', 'BRAWO! WERSJA FINALNA ODDANA!'],
		'BRAWO! PROJEKT Z ASAPEM NIE BĘDZIE FAKAPEM!'
	];
	this.message = '';
	this.messageVisbile = false;
	this.messageText = null;
	this.messageEndpoint = 10;
	this.messageIndex = 0;
	this.sound = null;
	this.programmer = null;
	this.coffeeMessages = ['Podwójne Espresso raz!', 'WoW!', 'Ale siekła!', 'Dedlajnie nadchodzę!'];
	this.coffeeMessageText = null;
	this.deadline = null;
	this.controls = null;
};

Game.Status.prototype = {
	init: function () {
	},

	create: function(platformHandler, cameraHandler, coffeeHandler, soundHandler, programmerHandler, playerHandler) {
		this.platformHandler = platformHandler;
		this.cameraHandler = cameraHandler;
		this.coffeeHandler = coffeeHandler;
		this.sound = soundHandler;
		this.programmer = programmerHandler;
		this.playerHandler = playerHandler;

		this.deadline = this.level.game.add.sprite(900, 1070, 'dealine');
		this.deadline.anchor.setTo(0.5, 1);
		this.deadline.visible = false;
		this.deadline.scale.setTo(1.5, 1.5);
		this.deadline.fixedToCamera = true;
		this.deadline.animations.add('base', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 14, true);
		this.deadline.animations.play('base');
		//this.playerHandler.player.addChild(this.deadline);

		var status = this;
		window.onkeydown = function() {
            if (status.level.game.input.keyboard.event.keyCode == 13){
                status.restartGame(status);
            }
        }		
		
		var poly = this.level.game.add.bitmapData(275, 330);
        poly.ctx.rect(0, 0, 275, 330);
        poly.ctx.fillStyle = '#FF33ff';
        poly.ctx.fill();
	    this.scoreFrame = this.level.game.add.sprite(1905, 1010, poly);
	    this.scoreFrame.anchor.setTo(1, 1);
	    this.scoreFrame.fixedToCamera = true;

		this.livesGroup = this.level.add.physicsGroup();
		var liveX = 1785;
		for(var i = 0; i < 3; i++) {
			var live = this.level.add.sprite(liveX, 850, 'live');
	        this.livesGroup.add(live);
	        live.anchor.set(1);
			live.fixedToCamera = true;
			liveX += 50;
		}

		var style = {font: 'bold 36px Joystix', fill: 'black', align: 'right', wordWrap: true, wordWrapWidth: 450};
		var messageStyle = {font: 'bold 48px Joystix', fill: 'black', align: 'center', wordWrap: true, wordWrapWidth: 1300, stroke: '#FF33ff', strokeThickness: 3};

		this.scoreLabel = this.level.game.add.text(1885, 750, "SCORE:", style);
		this.scoreLabel.anchor.set(1);
		this.scoreLabel.fixedToCamera = true;

		this.scoreText = this.level.game.add.text(1885, 800, this.score, style);
		this.scoreText.anchor.set(1);
		this.scoreText.fixedToCamera = true;

		this.bestScoreLabel = this.level.game.add.text(1885, 950, "BEST:", style);
		this.bestScoreLabel.anchor.set(1);
		this.bestScoreLabel.fixedToCamera = true;

		this.bestscoreText = this.level.game.add.text(1885, 1000, this.bestScore, style);
		this.bestscoreText.anchor.set(1);
		this.bestscoreText.fixedToCamera = true;

		this.codedLabel = this.level.game.add.text(1885, 100, "CODED:", style);
		this.codedLabel.anchor.set(1);
		this.codedLabel.fixedToCamera = true;

		this.platformsText = this.level.game.add.text(1885, 150, this.platforms, style);
		this.platformsText.anchor.set(1);
		this.platformsText.fixedToCamera = true;

		this.messageText = this.level.game.add.text(this.level.game.world.centerX, 1600, this.message, messageStyle);
		this.messageText.anchor.set(0.5);
		var idx = this.level.rnd.integerInRange(0, 2);
		this.message = this.messages[this.messageIndex]['m'][idx];
		
		this.coffeeMessageText = this.level.game.add.text(this.level.game.world.centerX, 1600, this.message, messageStyle);
		this.coffeeMessageText.anchor.set(0.5);
	},

	deadlineCatch: function(player, deadline) {
		this.level.game.paused = true;
		this.lives--;
		var status = this;
		var interval = setInterval(function(){ 
			status.restartGame(status);
		}, 5000);
	},

	restartGame: function(game) {
		if(game.level.game.paused) {
			game.level.game.paused = false;
			if(game.lives > 0) {
				game.livesGroup.getFirstAlive().destroy();
				game.cameraHandler.reinit();
				game.platformHandler.reinit();
				game.coffeeHandler.reinit();
				game.playerHandler.startPlayer();

				game.messageVisbile = false;
				game.messageEndpoint = 10;
				game.messageIndex = 0;
				game.messageText.y = 1600;
				var idx = game.level.rnd.integerInRange(0, 2);
				game.message = game.messages[this.messageIndex]['m'][idx];
				game.messageText.update();

				game.deadline.visible = false;
			} else {
				window.location.href = '/game/index1.php?form&points=' + this.bestScore;
			}
		}
	},

	update: function() {
		this.scoreLabel.setText("SCORE:");
		this.bestScoreLabel.setText("BEST:");
		this.codedLabel.setText("CODED:");

		this.score = this.platformHandler.platformsScore;
		if(this.score > this.bestScore) {
			this.bestScore = this.score;
		}
		this.scoreText.setText(this.score);
		this.bestscoreText.setText(this.bestScore);

		this.platforms = this.platformHandler.platformsDone;
		this.platformsPercent = parseFloat((this.platforms / 120) * 100).toFixed(2);
		this.platformsText.setText(this.platformsPercent + ' %');

		if(this.cameraHandler.gameOver) {
			this.sound.gameOver();
			this.programmer.error();

			//stop player
			this.playerHandler.stopPlayer();
			this.cameraHandler.cameraStep = 2000;

			//deadline
			if(!this.deadline.visible) {
				this.deadline.x = this.platformHandler.player.x;
				this.deadline.visible = true;

				var y = this.level.camera.y;
				this.messageText.y = y;
				var tween = this.level.game.add.tween(this.messageText).to({y: y + 100}, 500, Phaser.Easing.Bounce.Out, true);
				tween.onComplete.add(function() {
					this.messageVisbile = false;
				}, this);
				this.messageVisbile = true;
				var m = this.endMessages[0];
				if(this.platformsPercent > 100 && this.platformsPercent < 200) {
					var idx = this.level.rnd.integerInRange(0, 6);
					m = this.endMessages[1][idx];
				}
				if(this.platformsPercent >= 200) {
					m = this.endMessages[2];
				}
				var cm = '\n';
				if(this.lives == 1) {
					cm = '\nKoniec gry';
				}
				this.messageText.setText(m + '\nZrobione ' + this.platformsPercent + '% projektu.' + cm);
				this.messageText.bringToTop();
			} else {
				this.deadline.cameraOffset.x = this.platformHandler.player.x;
				this.deadline.bringToTop();
				this.deadline.update();
			}
		}

		//messages
		if(this.platformsPercent > this.messageEndpoint && !this.messageVisbile) {
			this.messageVisbile = true;
			this.messageText.setText(this.message);
			this.messageText.y = this.level.camera.y + 1600;
			var tween = this.level.game.add.tween(this.messageText).to({y: this.level.camera.y - 500}, 2000, Phaser.Easing.Bounce.Out, true);
			tween.onComplete.add(function() {
				this.messageVisbile = false;
				this.messageIndex++;

				if(this.messageIndex < 15) {
					this.messageEndpoint = this.messages[this.messageIndex]['p'];
					var idx = this.level.rnd.integerInRange(0, 2);
					this.message = this.messages[this.messageIndex]['m'][idx];
				}
			}, this);
		}
		
		if(this.coffeeHandler.showMessage) {
			this.coffeeHandler.showMessage = false;
			var y =this.coffeeMessageText.y = this.level.camera.y - 1000;
			var tween = this.level.game.add.tween(this.coffeeMessageText.scale).to({x: 2.5, y: 2.5}, 1000, Phaser.Easing.Linear.None, true);
			this.level.game.add.tween(this.coffeeMessageText).to({y: y - 1000}, 1500, Phaser.Easing.Linear.None, true, true);
			tween.onComplete.add(function() {
				this.coffeeMessageText.scale.x = 1;
				this.coffeeMessageText.scale.y = 1;
				//this.messageVisbile = false;
			}, this);
			//this.messageVisbile = true;
			var idx = this.level.rnd.integerInRange(0, 3);
			var m = this.coffeeMessages[idx];
			this.coffeeMessageText.setText(m);
		}
	}
}
