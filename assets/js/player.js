Game.Player = function(level) {
	this.level = level;
	this.player = null;
	this.sound = null;
	this.control = {};
	this.playerSpeed = 400;
	this.jumpTimer = 0;
	this.moveTimer = 0;
	this.isRunning = false;
	this.runningTime = 0;
	this.jumpStrength = 650;
	this.facing = 'front';
	this.programmer = null;
	this.pad1 = null;
	this.padMoveTimer = 0;
	this.downTimer = 0;
	this.deltaTimer = 0;
	this.bgRun = false;
};

Game.Player.prototype = {
	init: function () {
	},

	create: function(sound, programmer) {
		this.sound = sound;
		this.player = this.level.add.sprite(960, 900, 'player');
		this.player.anchor.setTo(0.5, 0.5);
		this.programmer = programmer;
		console.log(this.player);

		this.player.animations.add('stay', [7, 8, 9, 10, 11, 12, 13], 3, true);
		this.player.animations.add('front', [3], 1, true);
		this.player.animations.add('jump', [15, 14], 2, false);
		this.player.animations.add('jumpLeft', [2], 1, true);
		this.player.animations.add('left', [0, 1], 7, true);
		this.player.animations.add('jumpRight', [4], 1, true);
        this.player.animations.add('right', [5, 6], 7, true);
		
		this.player.animations.play('stay');

		this.controls = {
			'right': this.level.input.keyboard.addKey(Phaser.Keyboard.D),
			'left': this.level.input.keyboard.addKey(Phaser.Keyboard.A),
			'up': this.level.input.keyboard.addKey(Phaser.Keyboard.W),
		};

		this.level.game.input.gamepad.start();
		this.pad1 = this.level.game.input.gamepad.pad1;
		this.level.game.input.onDown.add(this.dump, this);
	},

	dump: function() {
		console.log(this.pad1._axes[0]);
    	console.log(this.pad1._rawPad.axes[0]);
	},

	update: function() {
		var speed = this.playerSpeed + this.runningTime / 5;
		if(this.controls.up.isDown || this.pad1.isDown(Phaser.Gamepad.XBOX360_B)) {
			this.player.animations.play('jump');
			
		}
		if(this.controls.right.isDown || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
			this.programmer.code();
			
			if(this.controls.up.isDown || !this.player.body.touching.down) {
				this.player.animations.play('jumpRight');
			} else {
				this.player.animations.play('right');
				
				//this.runningTime = this.controls.right.duration;
			}
			this.runningTime += 10;		
			this.facing = 'right';
			this.player.body.velocity.x += speed;
			//this.moveTimer = this.controls.right.timeDown;
			//this.moveTimer++;
			//this.isRunning = !this.controls.right.downDuration(50);
		} else if(this.controls.left.isDown || this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
			this.programmer.code();
			if(this.controls.up.isDown || !this.player.body.touching.down) {
				this.player.animations.play('jumpLeft');
			} else {
				this.player.animations.play('left');
				
				//this.runningTime = this.controls.right.duration;
			}
			this.runningTime += 10;
			this.facing = 'left';
			this.player.body.velocity.x -= speed;
			//this.moveTimer = this.controls.left.timeDown;
			//this.moveTimer++;
			//this.isRunning = !this.controls.left.downDuration(50);
		} else {
			
			
			if (this.facing !== 'front')
            {
            	if(this.player.body.onFloor() || this.player.body.touching.down) {
            		this.programmer.wait();
            	}
            	
                this.player.animations.stop();

                if (this.facing === 'left')
                {
                    this.player.frame = 4;
                }
                else
                {
                    this.player.frame = 2;
                }

                this.facing = 'front';
                this.player.frame = 3;
            }
		}
		if((this.controls.up.isDown || this.pad1.isDown(Phaser.Gamepad.XBOX360_B)) && (this.player.body.onFloor() || this.player.body.touching.down) && this.level.time.now > this.jumpTimer) {
			this.programmer.code();
			
			this.sound.jump();
			//console.log(this.runningTime);
			var y = this.jumpStrength;
			if(this.isRunning) {
				
				var s = this.runningTime;
				/*if(s > 1500) {
					s = 1500;
				} else */if(s < 0) {
					s = 0;
				}
				y += (s / 2);
			} 
			//console.log(y);
			this.player.body.velocity.y = -y;
			this.jumpTimer = this.level.time.now + 800;
			this.runningTime += 700;

			//this.controls.up.reset();
		}

		/*if((this.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) == 0)) {
			//this.padMoveTimer = 0;
			//this.moveTimer = 0;
			//if(this.level.time.now > this.jumpTimer) {
			if((this.player.body.onFloor() || this.player.body.touching.down))
				this.runningTime *= 0.3;
			//}
			/*if(this.level.time.now > this.jumpTimer) {
				this.runningTime = 0;
			}*
		}*/
		if(this.player.body.onFloor() || this.player.body.touching.down) {
			this.runningTime *= 0.99;
		}
		this.runningTime *= 0.99;

		this.player.bringToTop();
		this.isRunning = true;
		//this.runningTime = 0;

		var playerDeltaY = this.playerLastY - this.player.y;
		if(playerDeltaY == 0) {
			$('body').stop();
			this.bgRun = false;
		} else {
			if(!this.bgRun) {
				animatePosition('body', true);
				this.bgRun = true;
			}
		}
		/*animatePosition = function(that) {
		    $(that).css({
		        'background-position-y': '0%'
		    }).animate({
		        'background-position-y': '150%'
		    }, 6000, 'linear', function(){
		        animatePosition(that);
		    });
		};*/

		if(this.level.time.now > this.deltaTimer){
	    	this.playerLastY = this.player.y;
	    	this.deltaTimer = this.level.time.now + 1000;
	    }
	},

	setPhysics: function() {
		this.player.body.collideWorldBounds = true;
		//this.player.body.addCapsule(88, 132, 0, 0, 90);
	},

	getPlayer: function() {
		return this.player;
	},

	stopPlayer: function() {
		this.player.body.allowGravity = false;
		this.player.body.velocity.x = 0;
		this.player.body.velocity.y = 50;
		this.player.body.immovable = true;
        this.player.update();
	},

	startPlayer: function() {
		this.player.body.allowGravity = true;
		this.player.body.velocity.x = this.playerSpeed;
		this.player.body.velocity.y = this.playerSpeed;
		this.player.body.immovable = false;
		this.runningTime = 0;
		this.jumpTimer = 0;
        this.player.update();
        this.programmer.wait();
        this.bgRun = false;
	}
}