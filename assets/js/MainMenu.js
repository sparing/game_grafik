Game.MainMenu = function(game) {
	this.player = null;
	this.game = game;
	this.pad1 = null;
};

Game.MainMenu.prototype = {
	preload:function() {		
		this.load.spritesheet('dealine_anim', 'assets/dedlajn_grafik_animacja.png', 405, 214);
		this.load.image('menu', 'assets/preloader.png');
		this.load.spritesheet('start_button', 'assets/btn_graj.png', 242, 114);
	},

	create: function () {
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	    this.scale.maxWidth = this.game.width;
	    this.scale.maxHeight = this.game.height;
	    this.scale.pageAlignHorizontally = true;
	    this.scale.pageAlignVertically = true;

		this.stage.backgroundColor = '#ffffff';
		
		
		

		this.player = this.add.sprite(960, 900, 'dealine_anim');
		this.player.anchor.setTo(0.5, 0.5);
		this.player.animations.add('run', [0, 1, 2, 3, 4, 5], 3, true);
		this.player.animations.play('run');
		
		/*this.start_btn = this.add.sprite(800, 500, 'start_button');
		this.player.anchor.setTo(0.5, 0.5);
		this.player.animations.add('snap', [0, 1], 2, true);
		this.player.animations.play('snap');*/
		
		var style = {font: 'bold 61px Joystix', fill: 'black', align: 'center', wordWrap: true, wordWrapWidth: 820};
		//var messageStyle = {font: 'bold 48px Joystix', fill: 'black', align: 'right', wordWrap: true, wordWrapWidth: 1000, stroke: '#FF33ff', strokeThickness: 3};
		var style1 = {font: '61px FreePixel', fill: 'black', align: 'left', wordWrap: true, wordWrapWidth: 1220};
		
		this.game.time.events.add(5, function(){
			this.title = this.game.add.text(950, 200, "GRAFIK \nDZIKI DEADLINE", style);
			this.title.anchor.set(0.5, 0.5);
		
		
			this.desc = this.game.add.text(350, 340, "Wciel się w postać grafika i uciekaj przed dzikim dedlajnem! Pomoże Ci Twój ziomek Programista!", style1);
			this.desc.anchor.set(0, 0);
		}, this);
			
		this.add.button(820, 650, 'start_button', this.startGame, this);

		this.physics.arcade.enable(this.player);
		this.player.body.allowGravity = false;

		this.game.input.gamepad.start();
		this.pad1 = this.game.input.gamepad.pad1;
	},

	update: function() {
		this.player.body.x += 6;
		if(this.player.body.x > this.game.width) {
			this.player.body.x = -100;
		}
		/*this.game.physics.arcade.moveToXY(
		    this.player, 
		    this.player.body.x - 70,
		    this.player.body.y,
		    100
		);*/
		if(this.pad1.isDown(Phaser.Gamepad.XBOX360_START)) {
			this.startGame();
		}
	},

	startGame: function () {		
		this.state.start('Preloader');
	}
}